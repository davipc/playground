package service;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import injected.InjectThis;

@Named(value="testService")
public class TestService {
	private static Logger logger = LoggerFactory.getLogger(TestService.class);
	
	@Inject
	private InjectThis injected;
	
	public String callInjected() {
		return "Random = " + injected.getRandom();
	}
	
	
	// using imported weld-se to run application containing injections in JavaSE mode
	public static void main(String[] args) {
		Weld weld = new Weld();
	    WeldContainer container = weld.initialize();
	    TestService testService = container.instance().select(TestService.class).get();
	    logger.info("Received this from injected service call: " + testService.callInjected());
	    weld.shutdown();		
	}
	
}
