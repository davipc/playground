package injected;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InjectThisImpl implements InjectThis {
	private static Logger logger = LoggerFactory.getLogger(InjectThis.class);
	
	private int attr1;
	private String attr2;
	
	public int getRandom() {
		
		attr1 = 1;
		attr2 = "testing";
		
		logger.info(attr1 + " " + attr2);
		return (int)(100*Math.random());
	}
}
