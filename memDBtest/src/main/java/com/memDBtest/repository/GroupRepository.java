package com.memDBtest.repository;

import org.springframework.data.repository.CrudRepository;

import com.memDBtest.entity.Group;

public interface GroupRepository extends CrudRepository<Group, Integer> {
}
