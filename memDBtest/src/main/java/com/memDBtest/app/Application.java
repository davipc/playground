package com.memDBtest.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = "com.memDBtest.entity")
@EnableJpaRepositories(basePackages = "com.memDBtest.repository")
public class Application extends SpringBootServletInitializer {

	private static Logger logger = LoggerFactory.getLogger(Application.class);
	
	// this method and extending the serlvet initializer are needed for running on Tomcat through a packaged war
	// for running in Tomcat from inside Eclipse, remember to also make the project a Dynamic Web Module (facets) 
	// and add Maven Dependencies to Deployment Assembly  
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }	
	
    public static void main(String[] args) throws Exception {
    	logger.info("Starting spring boot application...");
        SpringApplication.run(Application.class, args);
    	logger.info("Finished starting spring boot application");
    }
}
