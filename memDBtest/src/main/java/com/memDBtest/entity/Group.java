package com.memDBtest.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;
import lombok.ToString;

@Entity
@Table(name="Groups")

@Data
@NoArgsConstructor
@AllArgsConstructor(access=AccessLevel.PRIVATE)
@ToString(exclude="people")
@Builder
public class Group {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int id;
	
	public String name;
	
	@Singular("people")
	@OneToMany(mappedBy="group", fetch=FetchType.EAGER)
	private List<Person> people;
}
