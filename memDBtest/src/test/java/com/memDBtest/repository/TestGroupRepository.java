package com.memDBtest.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.memDBtest.app.Application;
import com.memDBtest.entity.Group;
import com.memDBtest.repository.GroupRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@DatabaseSetup(TestGroupRepository.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { TestGroupRepository.DATASET })
@DirtiesContext
@TestExecutionListeners({ 
		DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class 
})
public class TestGroupRepository {

	protected static final String DATASET = "classpath:datasets/all-entries.xml";

	@Autowired
	private GroupRepository repository;

	@Test
	public void testInsert() {
		Group group = Group.builder().name("Group 3").build();
		Group saved = repository.save(group);

		log.info("Group saved: " + saved);

		assertThat(group).isEqualToIgnoringGivenFields(saved, "id");
	}

	@Test
	public void testRead() {
		Group group = Group.builder().name("toRead").build();
		Group saved = repository.save(group);

		Group read = repository.findOne(saved.getId());

		log.info("Group found: " + read);

		assertThat(group).isEqualToIgnoringGivenFields(saved, "id", "people");
		assertThat(saved.getPeople()).hasSameElementsAs(read.getPeople());
		assertThat(saved).isEqualToIgnoringGivenFields(read, "people");
	}
	
	@Test
	public void testFindAll() {
		List<Group> allGroups = (List<Group>) repository.findAll();
		assertThat(allGroups).hasSize(2);
		for (Group group: allGroups) {
			log.debug("Found group: " + group);
			assertThat(group.getId()).isIn(new Object[]{-1, -2});
			if (group.getId() == -1) {
				assertThat(group.getPeople()).hasSize(2);
			} else {
				assertThat(group.getPeople()).hasSize(0);
			}
		}
		
	}

}
