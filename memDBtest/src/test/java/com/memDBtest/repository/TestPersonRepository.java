package com.memDBtest.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.memDBtest.app.Application;
import com.memDBtest.entity.Group;
import com.memDBtest.entity.Person;
import com.memDBtest.repository.PersonRepository;

import lombok.extern.slf4j.Slf4j; 

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class TestPersonRepository {
	
	@Autowired
	private PersonRepository repository;
	
	private static final Group g1 = Group.builder().name("Group 1").build();
	
	@Test
	public void testInsert() {
		Person person = Person.builder().name("Davi").value(123L).build();
		Person saved = repository.save(person);
		
		log.info("User saved: " + saved);
		
		assertThat(person).isEqualToIgnoringGivenFields(saved, "id");
	}
	
	@Test
	public void testRead() {
		Person person = Person.builder().name("toRead").value(45L).group(g1).build();
		Person saved = repository.save(person);
		
		Person read = repository.findOne(saved.getId());
		
		log.info("User found: " + read);
		
		assertThat(person).isEqualToIgnoringGivenFields(saved, "id");
		// saved will not have the list of people set - read will - need to check group object specifically
		assertThat(saved).isEqualToIgnoringGivenFields(read, "group");
		assertThat(saved.getGroup().getName()).isEqualTo(read.getGroup().getName());
	}
	
}
