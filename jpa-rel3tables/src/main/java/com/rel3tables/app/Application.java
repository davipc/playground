package com.rel3tables.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = "com.rel3tables.entity")
@EnableJpaRepositories(basePackages = "com.rel3tables.repository")
public class Application {

	private static Logger logger = LoggerFactory.getLogger(Application.class);
	
    public static void main(String[] args) throws Exception {
    	logger.info("Starting spring boot application...");
        SpringApplication.run(Application.class, args);
    	logger.info("Finished spring boot application");
    }
}
