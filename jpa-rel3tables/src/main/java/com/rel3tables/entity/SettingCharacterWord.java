package com.rel3tables.entity;

import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.ToString;


/**
 * The persistent class for the setting_character_word database table.
 * 
 */
@Entity
@Table(name="setting_character_word")
@AssociationOverrides({
	@AssociationOverride(name = "pk.wordId", 
		joinColumns = @JoinColumn(name = "WORD_ID")),
	@AssociationOverride(name = "pk.characterId", 
		joinColumns = @JoinColumn(name = "CHARACTER_ID")),
	@AssociationOverride(name = "pk.settingId", 
		joinColumns = @JoinColumn(name = "SETTING_ID")) })

@ToString(exclude={"pk", "character", "setting"})
public class SettingCharacterWord implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SettingCharacterWordPK pk;

	@Column(nullable=false)
	private Integer count;

	//bi-directional many-to-one association to Character
	@ManyToOne
	@JoinColumn(name="character_id", nullable=false, insertable=false, updatable=false)
	private Character character;

	//bi-directional many-to-one association to Setting
	@ManyToOne
	@JoinColumn(name="setting_id", nullable=false, insertable=false, updatable=false)
	private Setting setting;

	//bi-directional many-to-one association to Word
	@ManyToOne
	@JoinColumn(name="word_id", nullable=false, insertable=false, updatable=false)
	private Word word;

	public SettingCharacterWord() {
	}

	public SettingCharacterWordPK getId() {
		return this.pk;
	}

	public void setId(SettingCharacterWordPK id) {
		this.pk = id;
	}

	public Integer getCount() {
		return this.count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Character getCharacter() {
		return this.character;
	}

	public void setCharacter(Character character) {
		this.character = character;
	}

	public Setting getSetting() {
		return this.setting;
	}

	public void setSetting(Setting setting) {
		this.setting = setting;
	}

	public Word getWord() {
		return this.word;
	}

	public void setWord(Word word) {
		this.word = word;
	}

}