package com.rel3tables.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.ToString;


/**
 * The persistent class for the setting database table.
 * 
 */

@ToString
@Entity
public class Setting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(length=255)
	private String name;

//	//bi-directional many-to-one association to SettingCharacterWord
//	@OneToMany(mappedBy="setting")
//	private List<SettingCharacterWord> settingCharacterWords;
//
	//bi-directional many-to-many association to Character
	@ManyToMany(fetch=FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinTable(
		name="setting_character_word"
		, joinColumns={
			@JoinColumn(name="setting_id", nullable=false)
			}
		, inverseJoinColumns={
			@JoinColumn(name="character_id", nullable=false)
			}
		)
	private List<Character> characters;

	public Setting() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	public List<SettingCharacterWord> getSettingCharacterWords() {
//		return this.settingCharacterWords;
//	}
//
//	public void setSettingCharacterWords(List<SettingCharacterWord> settingCharacterWords) {
//		this.settingCharacterWords = settingCharacterWords;
//	}
//
//	public SettingCharacterWord addSettingCharacterWord(SettingCharacterWord settingCharacterWord) {
//		getSettingCharacterWords().add(settingCharacterWord);
//		settingCharacterWord.setSetting(this);
//
//		return settingCharacterWord;
//	}
//
//	public SettingCharacterWord removeSettingCharacterWord(SettingCharacterWord settingCharacterWord) {
//		getSettingCharacterWords().remove(settingCharacterWord);
//		settingCharacterWord.setSetting(null);
//
//		return settingCharacterWord;
//	}

	public List<Character> getCharacters() {
		return this.characters;
	}

	public void setCharacters(List<Character> characters) {
		this.characters = characters;
	}

}