package com.rel3tables.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.ToString;


/**
 * The persistent class for the character database table.
 * 
 */
@Entity

@ToString
public class Character implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(length=255)
	private String name;

	//bi-directional many-to-one association to SettingCharacterWord
	@OneToMany(mappedBy="character", fetch=FetchType.EAGER)
	private List<SettingCharacterWord> settingCharacterWords;

	//bi-directional many-to-many association to Setting
//	@ManyToMany(mappedBy="characters")
//	private List<Setting> settings;

	//bi-directional many-to-many association to Word
//	@ManyToMany
//	@JoinTable(
//		name="setting_character_word"
//		, joinColumns={
//			@JoinColumn(name="character_id", nullable=false)
//			}
//		, inverseJoinColumns={
//			@JoinColumn(name="word_id", nullable=false)
//			}
//		)
//	private List<Word> words;

	public Character() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SettingCharacterWord> getSettingCharacterWords() {
		return this.settingCharacterWords;
	}

	public void setSettingCharacterWords(List<SettingCharacterWord> settingCharacterWords) {
		this.settingCharacterWords = settingCharacterWords;
	}

	public SettingCharacterWord addSettingCharacterWord(SettingCharacterWord settingCharacterWord) {
		getSettingCharacterWords().add(settingCharacterWord);
		settingCharacterWord.setCharacter(this);

		return settingCharacterWord;
	}

	public SettingCharacterWord removeSettingCharacterWord(SettingCharacterWord settingCharacterWord) {
		getSettingCharacterWords().remove(settingCharacterWord);
		settingCharacterWord.setCharacter(null);

		return settingCharacterWord;
	}
//
//	public List<Setting> getSettings() {
//		return this.settings;
//	}
//
//	public void setSettings(List<Setting> settings) {
//		this.settings = settings;
//	}
//
//	public List<Word> getWords() {
//		return this.words;
//	}
//
//	public void setWords(List<Word> words) {
//		this.words = words;
//	}
//
}