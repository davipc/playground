package com.rel3tables.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the setting_character_word database table.
 * 
 */
@Embeddable
public class SettingCharacterWordPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="character_id", insertable=false, updatable=false, nullable=false)
	private Integer characterId;

	@Column(name="setting_id", insertable=false, updatable=false, nullable=false)
	private Integer settingId;

	@Column(name="word_id", insertable=false, updatable=false, nullable=false)
	private Integer wordId;

	public SettingCharacterWordPK() {
	}
	public Integer getCharacterId() {
		return this.characterId;
	}
	public void setCharacterId(Integer characterId) {
		this.characterId = characterId;
	}
	public Integer getSettingId() {
		return this.settingId;
	}
	public void setSettingId(Integer settingId) {
		this.settingId = settingId;
	}
	public Integer getWordId() {
		return this.wordId;
	}
	public void setWordId(Integer wordId) {
		this.wordId = wordId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SettingCharacterWordPK)) {
			return false;
		}
		SettingCharacterWordPK castOther = (SettingCharacterWordPK)other;
		return 
			this.characterId.equals(castOther.characterId)
			&& this.settingId.equals(castOther.settingId)
			&& this.wordId.equals(castOther.wordId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.characterId.hashCode();
		hash = hash * prime + this.settingId.hashCode();
		hash = hash * prime + this.wordId.hashCode();
		
		return hash;
	}
}