package com.rel3tables.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.ToString;


/**
 * The persistent class for the word database table.
 * 
 */
@Entity

@ToString(exclude="id")
public class Word implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Column(length=255)
	private String word;

	//bi-directional many-to-one association to SettingCharacterWord
//	@OneToMany(mappedBy="word")
//	private List<SettingCharacterWord> settingCharacterWords;

	//bi-directional many-to-many association to Character
//	@ManyToMany(mappedBy="words")
//	private List<Character> characters;

	public Word() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getWord() {
		return this.word;
	}

	public void setWord(String word) {
		this.word = word;
	}

//	public List<SettingCharacterWord> getSettingCharacterWords() {
//		return this.settingCharacterWords;
//	}
//
//	public void setSettingCharacterWords(List<SettingCharacterWord> settingCharacterWords) {
//		this.settingCharacterWords = settingCharacterWords;
//	}
//
//	public SettingCharacterWord addSettingCharacterWord(SettingCharacterWord settingCharacterWord) {
//		getSettingCharacterWords().add(settingCharacterWord);
//		settingCharacterWord.setWord(this);
//
//		return settingCharacterWord;
//	}
//
//	public SettingCharacterWord removeSettingCharacterWord(SettingCharacterWord settingCharacterWord) {
//		getSettingCharacterWords().remove(settingCharacterWord);
//		settingCharacterWord.setWord(null);
//
//		return settingCharacterWord;
//	}
//
//	public List<Character> getCharacters() {
//		return this.characters;
//	}
//
//	public void setCharacters(List<Character> characters) {
//		this.characters = characters;
//	}
//
}