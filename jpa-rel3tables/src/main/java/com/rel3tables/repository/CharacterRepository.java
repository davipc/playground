package com.rel3tables.repository;

import org.springframework.data.repository.CrudRepository;

import com.rel3tables.entity.Character;

public interface CharacterRepository extends CrudRepository<Character, Integer> {
}
