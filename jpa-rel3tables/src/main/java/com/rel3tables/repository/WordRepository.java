package com.rel3tables.repository;

import org.springframework.data.repository.CrudRepository;

import com.rel3tables.entity.Word;

public interface WordRepository extends CrudRepository<Word, Integer> {
}
