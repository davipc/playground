package com.rel3tables.repository;

import org.springframework.data.repository.CrudRepository;

import com.rel3tables.entity.SettingCharacterWord;
import com.rel3tables.entity.SettingCharacterWordPK;

public interface SettingCharacterWordRepository extends CrudRepository<SettingCharacterWord, SettingCharacterWordPK> {
}
