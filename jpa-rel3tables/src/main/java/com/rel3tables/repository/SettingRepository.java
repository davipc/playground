package com.rel3tables.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rel3tables.entity.Setting;

public interface SettingRepository extends JpaRepository<Setting, Integer> {
}
