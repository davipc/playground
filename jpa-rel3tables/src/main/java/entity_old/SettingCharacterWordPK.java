package entity_old;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
//@Builder

@Embeddable
public class SettingCharacterWordPK implements Serializable {
	private static final long serialVersionUID = 1440809937905493508L;

	@ManyToOne(optional=false)
	private Setting setting;
	
	@ManyToOne(optional=false)
	private Character character;
	
	@ManyToOne(optional=false)
	private Word word;
	
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("SettingCharacterWordPK( ");
		sb.append("setting=Setting(" + (setting == null ? "null" : "id=" + setting.getId() + ", name=" + setting.getName() + ")")).append(", ");		
		sb.append("character=Character(" + (character == null ? "null" : "id=" + character.getId() + ", name=" + character.getName() + ")")).append(", ");
		sb.append("word=Word(" + (word == null ? "null" : "id=" + word.getId() + ", word=" + word.getWord() + ")"));
		sb.append(")");
		return sb.toString();
	}
}
