package entity_old;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SettingCharacterWord {

	@EmbeddedId
	private SettingCharacterWordPK id;
	
	private int count;
}
