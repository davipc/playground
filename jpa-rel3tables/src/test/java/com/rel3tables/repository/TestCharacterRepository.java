package com.rel3tables.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.rel3tables.app.Application;
import com.rel3tables.entity.Character;

import lombok.extern.slf4j.Slf4j; 

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@DatabaseSetup(TestCharacterRepository.DATASET)
@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { TestCharacterRepository.DATASET })
@DirtiesContext
@TestExecutionListeners({ 
		DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class 
})
public class TestCharacterRepository {
	protected static final String DATASET = "classpath:datasets/all-entries.xml";
	
	@Autowired
	private CharacterRepository repository;
	
	@Test
	public void testFindAll() {
		List<Character> allCharacters = (List<Character>) repository.findAll();
		
		log.info("Objects found: " + allCharacters.size());
		
		for (Character character: allCharacters) {
			System.out.println(character);
		}
		
		assertThat(allCharacters).hasSize(2);
	}
	
}
