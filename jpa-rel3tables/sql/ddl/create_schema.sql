-- Generated from PostgreSQL's PGAdmin

CREATE SEQUENCE public.hibernate_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.hibernate_sequence
  OWNER TO test;

CREATE TABLE setting
(
  id integer NOT NULL,
  name character varying(255),
  CONSTRAINT setting_pkey PRIMARY KEY (id)
);

CREATE TABLE "character"
(
  id integer NOT NULL,
  name character varying(255),
  CONSTRAINT character_pkey PRIMARY KEY (id)
);

CREATE TABLE word
(
  id integer NOT NULL,
  word character varying(255),
  CONSTRAINT word_pkey PRIMARY KEY (id)
);

CREATE TABLE public.setting_character_word
(
  setting_id integer NOT NULL,
  character_id integer NOT NULL,
  word_id integer NOT NULL,
  count integer NOT NULL,
  CONSTRAINT setting_character_word_pkey PRIMARY KEY (setting_id, character_id, word_id),
  CONSTRAINT fk_24cpddjuyvkl9blhc3btsff6n FOREIGN KEY (word_id)
      REFERENCES word (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_7detleum2ojjsk7ekk9cvjcw3 FOREIGN KEY (character_id)
      REFERENCES "character" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_9kx6m2aqgv3x9ao9lu5mbul0v FOREIGN KEY (setting_id)
      REFERENCES setting (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);