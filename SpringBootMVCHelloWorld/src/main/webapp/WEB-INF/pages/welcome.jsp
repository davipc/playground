<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <title>Getting Started: Serving Web Content</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
	<!-- let's add tag srping:url -->
	<spring:url value="/css/testapp.css" var="testappCSS" />
	<spring:url value="/js/testapp.js" var="testappJS" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<link href="${testappCSS}" rel="stylesheet" />
	<script src="${testappJS}"></script>
	<!-- Finish adding tags -->    
</head>
<body>
    <p>Hello, ${name}!</p>
    <br/>
    <div id="testappMsg"></div>
    <br/>
    <h2>Checkout this font color. Loaded from 'testapp.css' file under '/css' folder</h2>
    
</body>
</html>