package com.testapp.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

@SpringBootApplication
@ComponentScan(basePackages = "com.testapp.controller")
public class Application extends SpringBootServletInitializer { 

	private static Logger logger = LoggerFactory.getLogger(Application.class);
	
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

    @Bean  
    public UrlBasedViewResolver setupViewResolver() { 
    	logger.info("Setting up view resolver...");
    	
        UrlBasedViewResolver resolver = new UrlBasedViewResolver();  
        resolver.setPrefix("/WEB-INF/pages/");  
        resolver.setSuffix(".jsp");  
        resolver.setViewClass(JstlView.class);  
        return resolver;  
    }
    
    public static void main(String[] args) {
    	logger.info("Starting app...");

    	SpringApplication.run(Application.class, args);
    }
}