package com.test.lombok;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
	@NonNull private String name;
	@NonNull private String password;
	private int age;
	private User spouse;
	//@NonNull 
	private List<User> children;
	
	@SneakyThrows
	public static void main(String[] args) {
		
		User mel = new User("melissa", "001", 6, null, null);
		User levi = new User("levi", "002", 3, null, null);
		
		List<User> children = new ArrayList<>();
		children.add(mel);
		children.add(levi);
		
		User userRachel = new User("rachel", "012", 37, null, children);
		User userDavi = User.builder().name("davi").password("123").age(38).children(children).build();

		// LOMBOK doesn't handle cycles (Overflow when toString is called) - would have to specify each annotation separately instead of using @Data
		//userRachel.setSpouse(userDavi);
		//userDavi.setSpouse(userRachel);
		
		log.info("Davi: " + userDavi);
	
	}
}
