<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
<title>Spring MVC Tutorial - Hello World Spring MVC Example</title>

<!-- let's add tag srping:url -->
<spring:url value="/resources/testapp.css" var="testappCSS" />
<spring:url value="/resources/testapp.js" var="testappJS" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link href="${testappCSS}" rel="stylesheet" />
<script src="${testappJS}"></script>
<!-- Finish adding tags -->

</head>
<body>

	${message}
 
	<br>
	<br>
	<div style="font-family: verdana; padding: 10px; border-radius: 10px; font-size: 12px; text-align:center;">
 
		<h2>Checkout this font color. Loaded from 'testapp.css' file under '/WebContent/resources/' folder</h2>
		 
		<div id="testappMsg"></div>
		
		Spring MCV Tutorial by <a href="http://crunchify.com">Crunchify</a>.
		Click <a
			href="http://crunchify.com/category/java-web-development-tutorial/"
			target="_blank">here</a> for all Java and <a
			href='http://crunchify.com/category/spring-mvc/' target='_blank'>here</a>
		for all Spring MVC, Web Development examples.<br>
	</div>
</body>
</html>