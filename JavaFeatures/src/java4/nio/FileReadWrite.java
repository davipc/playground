package java4.nio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * Studied the tutorial at http://tutorials.jenkov.com/java-nio/index.html
 * 					   and https://docs.oracle.com/javase/tutorial/essential/io/file.html 
 * 
 * Buffers, Channels and Paths are the core of NIO
 * 
 * Differences in NIO: 
 * 
 *  	non-blocking IO: asynchronous reads/writes available for every channel (for instance: write request is made, while it happens thread can do something else)
 *		less threads required: Selectors can be used to manage multiple channel IO operations
 *		good when the input has a predefined format (always X bytes for the entire message, or X bytes for header and Y for body)
 *		buffers enable reading/writing on absolute positions without affecting concurrent reading of file
 * 
 * @author Davi
 *
 */
public class FileReadWrite {
	
	private static void copyFileTransfer(Path from, Path to) 
	throws IOException {
		// initialize IO for file read
		FileChannel fromChannel = (FileChannel)Files.newByteChannel(from, StandardOpenOption.READ);
		Set<OpenOption> openOptions = new HashSet<OpenOption>();
		// to enable creating the file in case it doesn't exist
		openOptions.add(StandardOpenOption.CREATE);
		openOptions.add(StandardOpenOption.WRITE);
		FileChannel toChannel = (FileChannel)Files.newByteChannel(to, openOptions);
		
		// file copy doesn't require a buffer 
		fromChannel.transferTo(0, fromChannel.size(), toChannel);
		toChannel.close();
		fromChannel.close();
	}
	
	private static void copyFileManual(Path from, Path to) 
	throws IOException {
		// initialize IO for file read
		FileChannel fromChannel = (FileChannel)Files.newByteChannel(from, StandardOpenOption.READ);
		ByteBuffer fromBuffer = ByteBuffer.allocate(10); 
		FileChannel toChannel = (FileChannel)Files.newByteChannel(to, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
		ByteBuffer toBuffer = ByteBuffer.allocate(30);
		
		int bytesRead = fromChannel.read(fromBuffer);
		while (bytesRead > 0) {
			
			//byte[] read = fromBuffer.array(); // this causes bytes past the limit to also be returned, so on last write & read, some junk bytes come through at the end.
			fromBuffer.flip();
			byte[] read = new byte[bytesRead]; 
			fromBuffer.get(read);
			
			toBuffer.clear();
			toBuffer.put(read);
			toBuffer.flip();
			toChannel.write(toBuffer);

			//Thread.sleep(1000);
			System.out.print(new String(read));
			
			fromBuffer.clear();
			bytesRead = fromChannel.read(fromBuffer);
		}
		toChannel.close();
		fromChannel.close();
	}
	
	private static void copyFileManualBuffered(Path from, Path to) 
	throws IOException {
		// initialize IO for file read
		BufferedReader reader = Files.newBufferedReader(from);
		BufferedWriter writer = Files.newBufferedWriter(to, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
		
		String lineSeparator = System.getProperty("line.separator");
		
		String nextLine; 
		while ((nextLine = reader.readLine()) != null) {
			writer.append(nextLine).write(lineSeparator);
		}
		
		reader.close();
		writer.close();
	}

	
	private static void writeFileManual(Path to) 
	throws IOException {
		// this should be used when you have all the lines to be written to the file
		ArrayList<String> lines = new ArrayList<String>();
		lines.add("This is the first line to be written to the new file");
		lines.add("Then comes the second line");
		lines.add("    and finally...    comes the third and last one!");

		Files.write(to, lines, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
	}
	
	
	public static void main(String[] args) throws Exception {
		Path from = Paths.get("d:/temp/nio/test.txt");
		Path to = Paths.get("d:/temp/nio/test2.txt");
		Path to2 = Paths.get("d:/temp/nio/test3.txt");
		Path to3 = Paths.get("d:/temp/nio/test4.txt");
		Path to4 = Paths.get("d:/temp/nio/test5.txt");

		copyFileTransfer(from, to);
		copyFileManual(from, to2);
		copyFileManualBuffered(from, to3);
		writeFileManual(to4);
	}
	
	
}
