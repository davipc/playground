package java5.minorChanges;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static java.lang.Double.parseDouble;

/**
 * Minor features covered by this class:
 * 
 * 		enhanced for loop
 * 		auto boxing / unboxing
 * 		formatter (System.out.format())
 * 		import static
 * 
 * @author Davi
 *
 */
public class MinorChanges {
	
	private static void enhancedForLoop() {
		// generic diamond notation below only came in on Java SE 7 
		List<String> ls = new ArrayList<>();
		ls.add("Hello");
		ls.add("World");
		
		// for loop
		for (String s: ls) {
			System.out.format("%s ", s);
		}
		System.out.println();
	}
	
	private static void autoBoxingUnboxing(int i, Long l, float f, Double d) {
		Integer i2 = i;
		long l2 = l;
		Float f2 = f;
		double d2 = d;
		
		// test some Formatter options as well
		// as well as import static
		System.out.format("%1$Td/%1$Tm/%1$TY %1$TT => %2$d %3$d %4$(,+4.5f %5$f %n", Calendar.getInstance(), i2 + 1, l2 + Long.valueOf("1"), f2 + 0.123, d2 + parseDouble("0.456"));
	}
	
	
	private static class Pair<K,V> {
		public K key;
		public V value;
		
		public Pair(K key, V value) {
			this.key = key;
			this.value = value;
		}
		
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("(").append(key).append(",").append(value).append(")");
			
			return sb.toString();
		}
	}
	
	
	// testing generics with wildcards at the same time 
	private static void varArgsList(List<?> ... lists) {
		System.out.println("Values provided: ");
		
		for (List<?> list: lists) {
			System.out.println(list);
		}
		System.out.println();
	}
	
	
	public static void main(String[] args) {
		enhancedForLoop();
		
		autoBoxingUnboxing(new Integer(10), 1234567890123L, new Float(1.1), 10.1);
		
		List<String> strList = new ArrayList<>();
		strList.add("first");
		strList.add("second");
		List<Integer> intList = new ArrayList<>();
		intList.add(1);
		List<Pair<Integer, String>> pairs = new ArrayList<>();
		pairs.add(new Pair<>(1, "Test"));
		pairs.add(new Pair<>(2, "Generic list"));
		pairs.add(new Pair<>(3, "JSE 5"));
		
		varArgsList(strList, intList, pairs);
	}
}
