package java5.generics;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Tutorial: 
 * 		http://www.journaldev.com/1663/java-generics-tutorial-example-class-interface-methods-wildcards-and-much-more
 * 		https://docs.oracle.com/javase/tutorial/java/generics/index.html
 *
 * Generics characteristics:
 * 		can be bounded or unbounded
 * 		multiple bounds are possible (<T extends A & B & C>), in which case only one of them can be a class
 *		there is no such thing as inheritance between bounded types (a List<Integer> cannot be cast to a List<Number> var, for instance)
 *			to solve that, wildcards must be used (List<? extends Number>)
 * 
 * Generics enable:
 * 		compile time check on parameter types
 * 		no need to cast return values
 * 		"templatizing" a method: no need to overload methods because of distinct input parameters, as long as they have the same mechanics (usually bounded)
 * 		
 * 
 * @author Davi
 *
 */
public class GenericMethods {
	
	public static <T extends Comparable<T>> T min(T first, T second) {
		T min;
		
		if (first.compareTo(second) < 0)
			min = first;
		else min = second;

		System.out.format("Min between %s and %s: %s%n", first , second, min);
		
		return min;
	}
	
	// show use of wildcards 
	
	public static double sumNumbersOnly(List<Number> list){
        double sum = 0;
        for(Number n : list){
            sum += n.doubleValue();
        }
        return sum;
    }

	// using wildcards to allow all types number lists
	public static double sum(List<? extends Number> list){
        double sum = 0;
        for(Number n : list){
            sum += n.doubleValue();
        }
        
        // we can't add a number to the list when using wildcards, though
        // list.add(new Integer(1)); // this won't compile
        
        System.out.println("Sum = " + sum);
        
        return sum;
    }
	
	// unbounded wildcard example
	// better than using List<Object>, as references to any types of lists will be accepted here 
	public static void printData(List<?> list){
		for(Object obj : list){
            System.out.println(obj + "::");
        }
    }
	
	// lower bound example: enables adding entries to the collection
	// in this example, because of the lower bound we know the list will take Integers 
	public static void addIntegers(List<? super Integer> list){
        list.add(new Integer(50));
    }	
	
	// subtyping is only possible with wildcards 
	public static void subtypeTest() {
		List<? extends Integer> intList = new ArrayList<>();
		List<? extends Number>  numList = intList;  // OK. List<? extends Integer> is a subtype of List<? extends Number>
		System.out.println(numList);
	}
	
	
	public static void main(String[] args) {

		// show use of template parameters
		System.out.println("Min tests:" );
		
		min(1, 1);
		min(1, 2);
		min(3, 2);
		min(1.0000001, 1.0000002);
		min(-1, -2);
		min("abd", "abf");
		min(new BigInteger("1234567891011121314151617181920"), new BigInteger("-1"));
		
		System.out.println();
		
		
		// show use of wildcards
		System.out.println("Sum tests:" );
		
		// don't need to set the type on the object instantiation if the compiler can infer the right type (in this case, it will obviously be Number).
		// can use the empty diamond instead: <>
		List<Number> numbers = new ArrayList<>();
		numbers.add(0);
		numbers.add(Double.MIN_VALUE);
		numbers.add(Long.MAX_VALUE);
		sumNumbersOnly(numbers);
		
		// this won't work
		List<Integer> integers = new ArrayList<Integer>();
		integers.add(0);
		integers.add(Integer.MAX_VALUE);
		integers.add(Integer.MIN_VALUE);
		
		// this won't work because there is no such thing as inheritance between bounded types 
		//d = sumNumbers(integers);
		
		// with the wildcard's version, both List<Number> and List<Integer> can be passed to the method
		sum(numbers);
		sum(integers);
		
		System.out.println();
		
		// test unbounded wildcard method
		printData(numbers);
		printData(integers);
		
		System.out.println();

		// test lower bounded wildcard
		addIntegers(numbers);
		addIntegers(integers);
		
		// test subtype assignment
		subtypeTest();
		
		
		
	}

}
