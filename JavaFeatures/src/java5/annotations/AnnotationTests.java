package java5.annotations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AnnotationTests {

	@MethodComment(
			purpose = "Test custom annotation"
	)
	public static void customAnnotationMethod() {
		System.out.println("This is a method with a methods only, custom annotation");
	}
	

	@Deprecated
	/**
	 * @deprecated
	 * This method tests the @Deprecated annotation
	 * @param i
	 */
	private static void deprecatedMethod(int i) {
		System.out.println("This is a deprecated method");
	}

	@Override
	public String toString() {
		return "test @Override";
	}
	
	@SuppressWarnings({"deprecation", "rawtypes", "unchecked"})
	public static void useDeprecated() {
		// doesn't generate warnings
		deprecatedMethod(2);
		
		Thread t = new Thread();
		// this is deprecated
		if (t.isAlive())
			t.destroy();
		
		List l = new ArrayList<Integer>();
		System.out.println(l);

		// this has both rawtypes and unchecked operations
		List l2 = new ArrayList();
		l2.add(15);
		int i = (Integer) l2.get(0);
		System.out.println(i);
	}
	
	// sometimes needed when var args (...) is used with generics
	@SafeVarargs
	private static <T> List<T> wrapInList(T... values) {
		List<T> type = new ArrayList<T>();
		
		for (T value: values)
			type.add(value);
		
		return type;
	}
	
	// not needed for this operation
	public static <T> void safeVarargs() {
		Set<Integer> aSet = new HashSet<Integer>();
		List<Set<Integer>> listOfSets = wrapInList(aSet);	
		System.out.println(listOfSets);
	}
	
	
	public static void main(String[] args) {
		customAnnotationMethod();
		deprecatedMethod(1);
		System.out.println(new AnnotationTests().toString());
		useDeprecated();
		safeVarargs();
	}
}
