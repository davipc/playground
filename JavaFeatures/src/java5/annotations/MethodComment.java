package java5.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

// causes the values to be used by javadocs
@Documented
@Target (value=ElementType.METHOD)
@interface MethodComment {
	String purpose();
	String[] parameters() default "";
	String[] exceptions() default "";
}
