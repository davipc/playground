package java5.scanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.MatchResult;

public class ScannerTest {
	
	public static void getParticipants() {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Please type the number of participants: ");
		int count = scanner.nextInt();
		
		ArrayList<String> part = new ArrayList<String>();
		if (count > 0) {
			for (int i = 0; i < count; i++) {
				System.out.print("Enter name of participant " + (i+1) + ": ");
				part.add(scanner.next());
			}
		}
		System.out.println("Thank you!");
		System.out.format("Provided list: %s", part.toString());
		
		scanner.close();		
	}

	public static void getParticipantsFromString(String participants, String delimiter) {
		Scanner scanner = new Scanner(participants);
		scanner.useDelimiter(delimiter);
		
		ArrayList<String> part = new ArrayList<String>();
		while (scanner.hasNext()) {
			part.add(scanner.next());
		}
		System.out.format("Provided list: %s%n", part.toString());

		scanner.close();
	}

	/**
	 * Test all results stored into MatchResult, then read all at once
	 * 
	 * @param participants
	 * @param delimiter
	 */
	public static void getParticipantsResultFromString(String participants, String delimiter) {
		Scanner scanner = new Scanner(participants);
		
		scanner.findInLine("(\\w+)" + delimiter + "(\\w+)" + delimiter + "(\\w+)" + delimiter + "(\\w+)");
		MatchResult result = scanner.match();
		
		ArrayList<String> part = new ArrayList<String>();
		for (int i = 1; i <= result.groupCount(); i++) {
			part.add(result.group(i));
		}
		System.out.format("Provided list: %s%n", part.toString());

		scanner.close();
	}

	// scanner can also read from files 
	public static void getParticipantsFromFile(String filename) 
	throws FileNotFoundException {
		File f = new File(filename);
		
		Scanner scanner = new Scanner(f);

		scanner.nextLine();
		
		ArrayList<String> part = new ArrayList<String>();
		while (scanner.hasNextLine()) {
			String nextLine = scanner.nextLine();
			part.add(nextLine);
		}
		System.out.format("Provided list: %s%n", part.toString());

		
		scanner.close();
	}
	
	public static void main(String[] args) 
	throws FileNotFoundException {
		
		//getParticipants();
		getParticipantsFromString("Rachel|Mel|Levi", "\\|");
		getParticipantsResultFromString("Mel|Levi|Rachel|Davi", "\\|");
		
		getParticipantsFromFile("d:/temp/nio/ScannerInput.txt");
		
	}
}
