package java5.enums;


public class Main {
	public static void main(String[] args) {
		
		System.out.println("Available heroes:");
		for (Hero h: Hero.values()) {
			System.out.format("%1$s (%3$s) -> nr %2$d in the list%n", h, h.ordinal(), h.getInitials());
		}
		System.out.println();
		
		Hero h = Hero.THOR;
		System.out.format("Chosen: %1$s (%3$s) -> nr %2$d in the list%n", h, h.ordinal(), h.getInitials());
		
		h = Hero.valueOf("HULK");
		System.out.format("ValueOf: %1$s (%3$s) -> nr %2$d in the list%n", h, h.ordinal(), h.getInitials());
	}
}