package java5.enums;

public enum Hero {
	
	THOR ("T"),
	IRON_MAN ("IM"),
	HULK ("H"),
	CAPTAIN_AMERICA ("CA");
	
	private String initials;
	
	private Hero(String initials) {
		this.initials = initials;
	}

	public Object getInitials() {
		return initials;
	}
	
	
	
	
}

