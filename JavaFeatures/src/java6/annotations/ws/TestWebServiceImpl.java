package java6.annotations.ws;

import javax.jws.WebService;


/**
 *
 * Done following tutorial at 
 * 	http://www.java2blog.com/2013/03/jaxws-web-service-eclipse-tutorial.html
 * 
 * - create server: 
 * 		- Create annotated service class and publish class
 * 		- run publish class (no need of containers yet, java 6 handles it)
 * - create client code:
 * 		- run wsimport pointing to published WSDL to create stubs
 * 		- create client class    
 * 
 * @author Davi
 *
 */
@WebService(endpointInterface="java6.annotations.ws.TestWebService")
public class TestWebServiceImpl implements TestWebService {

	@Override
	public int daysSince1970Jan1st() {
		int result = (int) (System.currentTimeMillis() / (1000 * 60 * 60 * 24));
		
		return result;
	}
	
	public static void main(String[] args) {
		TestWebServiceImpl tws = new TestWebServiceImpl();
		System.out.println(tws.daysSince1970Jan1st());
	}
	
}
