package java6.annotations.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;


/**
 *
 * Done following tutorial at 
 * 	http://www.java2blog.com/2013/03/jaxws-web-service-eclipse-tutorial.html
 * 
 * Without a Container (Tomcat for instance)
 * 
 * - create server: 
 * 		- Create annotated service class and publish class
 * 		- run publish class (no need of containers yet, java 6 handles it)
 * - create client code:
 * 		- run wsimport pointing to published WSDL to create stubs
 * 		- create client class    
 * 
 * With a container:
 * 
 * - create server:
 * 		- create annotated service class ONLY
 * 
 * @author Davi
 *
 */
@WebService
public interface TestWebService {

	@WebMethod
	public int daysSince1970Jan1st();
}
