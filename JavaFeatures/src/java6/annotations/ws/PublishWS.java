package java6.annotations.ws;

import javax.xml.ws.Endpoint;

public class PublishWS {

	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8080/testWS", new TestWebServiceImpl());
	}
}
