package java6.stax;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;

public class StaxTests {
	
	public static void testCursorAPI() 
	throws XMLStreamException, FileNotFoundException {
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLStreamReader reader = inputFactory.createXMLStreamReader(new FileReader("txt/stax.xml"));

		// Reads the entire XML element by element
		// if we reach an element we are looking for we can act on it
		// advantage over DOM: doesn't load all XML into memory
		// advantage over SAX: instead of registering callbacks when XML elements are found, it navigates through the XML acting as needed 
		// (application controls the XML reading) 
		while(reader.hasNext()) {
			Integer eventType = reader.next();
            if (eventType.equals(XMLEvent.START_ELEMENT)){
                System.out.print("<" + reader.getName());
                for (int i = 0; i < reader.getAttributeCount(); i++) {
                	System.out.format(" %s = \"%s\" ",reader.getAttributeName(i), reader.getAttributeValue(i));
                }
                System.out.print(">");
            } else if (eventType.equals(XMLEvent.CHARACTERS)){
                System.out.print(" " + reader.getText() + " ");
            } else if (eventType.equals(XMLEvent.ATTRIBUTE)){
            	System.out.print("!!! FOUND AN ATTRIBUTE !!!");
                System.out.print(" " + reader.getName() + " ");
            } else if (eventType.equals(XMLEvent.END_ELEMENT)){
                System.out.print("</" + reader.getName() + ">");
            }
        }
		System.out.println();
        reader.close();
	}
	
	public static void testEventIteratorAPI() 
	throws XMLStreamException, FileNotFoundException {
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLEventReader reader = inputFactory.createXMLEventReader(new FileReader("txt/stax.xml"));

		// Reads the entire XML event by event
		// if we reach an element we are looking for we can act on it
		// advantage over DOM: doesn't load all XML into memory
		// advantage over SAX: instead of registering callbacks when XML elements are found, it navigates through the XML acting as needed 
		// (application controls the XML reading) 
		while(reader.hasNext()) {
			XMLEvent event = reader.nextEvent();
            if (event.isStartElement()){
                System.out.print("<" + event.asStartElement().getName());
                @SuppressWarnings("unchecked")
				Iterator<Attribute> iterator = event.asStartElement().getAttributes();
                while (iterator.hasNext()) {
                	Attribute attr = iterator.next();
                	System.out.format(" %s = \"%s\" ",attr.getName(), attr.getValue());
                }
                System.out.print(">");
            } else if (event.isCharacters()){
                System.out.print(" " + event.asCharacters().getData() + " ");
            } else if (event.isEndElement()){
                System.out.print("</" + event.asEndElement().getName() + ">");
            }
        }
		System.out.println();
		
        reader.close();
	}
	
	public static void main(String[] args) 
	throws Exception {
		testCursorAPI();
		
		testEventIteratorAPI();		
	}
	
}
